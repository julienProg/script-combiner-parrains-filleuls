# script-combiner-parrains-filleuls



## Comment l'utiliser

1- Il suffit d'ouvrir le ficher HTML `final.html` sur un navigateur (Google chrome, Edge, Firefox, ...).

2- Ouvrir ou glisser le ficher excel dans l'endroit indiquer sur le navigateur

3- Copier le resultat obtenue dans la page du excel `Combinaison ParrainsMarraines -`

## Important

> La `syntaxe` dans le excel est importante (Majuscule, espace, ..).

### Pages
Le excel file doit avoir 3 pages:
```sh
 "ParrainsMarraines", "Filleul.es", "Combinaison ParrainsMarraines -"
 ```

### ParrainsMarraines
La page ParrainsMarraines est constitué de ses entêtes: (L'ordre est important)
```sh
"Nom", "Adresse courriel", "Genre", "Année au Pharm. D", "Langues parlées", "Langue préférée", "Région d'origine", "Moyen de transport", "Travailles en pharmacie ", "Avant le Pharm.D", "Après les études", "Déjà fait du parrainage ", "Nbr de filleul.es"
```
Remarque:

> `Genre` a été ajouté

> `Travailles en pharmacie ` et `Déjà fait du parrainage ` ont un espace à la fin.

### Filleul.es
La page Filleul.es est constitué de ses entêtes: (L'ordre est important)
```sh
"Nom", "Adresse courriel", "# téléphone", "Préférence de jumelage", "Langues parlées", "Langue préférée", "Région d'origine", "Moyen de transport", "Travailles en pharmacie durant les études ", "Informations à discuter", "Passe-temps favori", "Intérêt pour l'AEPUM", "Avant le Pharm.D", "Après les études"
```
Remarque:

> `Travailles en pharmacie durant les études ` a un espace à la fin.

> S'assurer que `Avant le Pharm.D` ait les même valeurs que `Avant le Pharm.D` dans Parrain
